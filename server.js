// Set up
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var cors = require('cors');
 
// Configuration
mongoose.connect('mongodb://localhost/developers');
 
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());
 
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});
 
// Models
var Developer = mongoose.model('Developers', {
    name: String,
    age: Number,
    languages: [],
    rating: Number
});
 
// Routes
 
 // Get developers
    app.get('/api/developers', function(req, res) {
 
        console.log("fetching Developers");
 
        // use mongoose to get all reviews in the database
        Developer.find(function(err, developers) {
 
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)
 
            res.json(developers); // return all reviews in JSON format
        });
    });
 
    // create review and send back all reviews after creation
    app.post('/api/developers', function(req, res) {
 
        console.log("creating Developer");
 
        // create a review, information comes from request from Ionic
        Developer.create({
            name : req.body.name,
            age : req.body.age,
	    languages: req.body.languages,
            rating: req.body.rating,
            done : false
        }, function(err, developer) {
            if (err)
                res.send(err);
 
            // get and return all the reviews after you create another
            Developer.find(function(err, developers) {
                if (err)
                    res.send(err)
                res.json(developers);
            });
        });
 
    });
 
    // delete a review
    app.delete('/api/reviews/:review_id', function(req, res) {
        Review.remove({
            _id : req.params.review_id
        }, function(err, review) {
 
        });
    });
 
 
// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");